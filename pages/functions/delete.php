<?php
	
	//SEMESTER

	if(isset($_POST['btn_deletesem']))
	{
	    if(isset($_POST['chk_delete']))
	    {
	        foreach($_POST['chk_delete'] as $value)
	        {
	            $delete_query = mysqli_query($con,"DELETE from tblsemester where id = '$value' ") or die('Error: ' . mysqli_error($con));
	                    
	            if($delete_query == true)
	            {
	                $_SESSION['delete'] = 1;
	                header("location: ".$_SERVER['REQUEST_URI']);
	            }
	        }
	    }
	}

	//SCHOOLYEAR

	if(isset($_POST['btn_deletesy']))
	{
	    if(isset($_POST['chk_delete']))
	    {
	        foreach($_POST['chk_delete'] as $value)
	        {
	            $delete_query = mysqli_query($con,"DELETE from tblschoolyear where id = '$value' ") or die('Error: ' . mysqli_error($con));
	                    
	            if($delete_query == true)
	            {
	                $_SESSION['delete'] = 1;
	                header("location: ".$_SERVER['REQUEST_URI']);
	            }
	        }
	    }
	}


	//FACULTY

	if(isset($_POST['btn_deletefac']))
	{
	    if(isset($_POST['chk_delete']))
	    {
	        foreach($_POST['chk_delete'] as $value)
	        {
	            $delete_query = mysqli_query($con,"DELETE from tblfaculty where id = '$value' ") or die('Error: ' . mysqli_error($con));
	                    
	            if($delete_query == true)
	            {
	                $_SESSION['delete'] = 1;
	                header("location: ".$_SERVER['REQUEST_URI']);
	            }
	        }
	    }
	}

	//STUDENT

	if(isset($_POST['btn_deletestud']))
	{
	    if(isset($_POST['chk_delete']))
	    {
	        foreach($_POST['chk_delete'] as $value)
	        {
	            $delete_query = mysqli_query($con,"DELETE from tblstudent where id = '$value' ") or die('Error: ' . mysqli_error($con));
	                    
	            if($delete_query == true)
	            {
	                $_SESSION['delete'] = 1;
	                header("location: ".$_SERVER['REQUEST_URI']);
	            }
	        }
	    }
	}

	//FILE CATEGORY

	if(isset($_POST['btn_deletefilecat']))
	{
	    if(isset($_POST['chk_delete']))
	    {
	        foreach($_POST['chk_delete'] as $value)
	        {
	            $delete_query = mysqli_query($con,"DELETE from tblfilecategory where id = '$value' ") or die('Error: ' . mysqli_error($con));
	                    
	            if($delete_query == true)
	            {
	                $_SESSION['delete'] = 1;
	                header("location: ".$_SERVER['REQUEST_URI']);
	            }
	        }
	    }
	}


	//DOWNLOADABLE FILE

	if(isset($_POST['btn_deletedownload']))
	{
	    if(isset($_POST['chk_delete']))
	    {
	        foreach($_POST['chk_delete'] as $value)
	        {
	            $delete_query = mysqli_query($con,"DELETE from tbldownloadable where id = '$value' ") or die('Error: ' . mysqli_error($con));
	                    
	            if($delete_query == true)
	            {
	                $_SESSION['delete'] = 1;
	                header("location: ".$_SERVER['REQUEST_URI']);
	            }
	        }
	    }
	}

		//STUDENT DOWNLOADABLE FILE

	if(isset($_POST['btn_deletestuddownload']))
	{
	    if(isset($_POST['chk_delete']))
	    {
	        foreach($_POST['chk_delete'] as $value)
	        {
	            $delete_query = mysqli_query($con,"DELETE from tblstuddownloadable where id = '$value' ") or die('Error: ' . mysqli_error($con));
	                    
	            if($delete_query == true)
	            {
	                $_SESSION['delete'] = 1;
	                header("location: ".$_SERVER['REQUEST_URI']);
	            }
	        }
	    }
	}



	//ACTIVITY

	if(isset($_POST['btn_deleteact']))
	{
	    if(isset($_POST['chk_delete']))
	    {
	        foreach($_POST['chk_delete'] as $value)
	        {
	            $delete_query = mysqli_query($con,"DELETE from tblactivities where id = '$value' ") or die('Error: ' . mysqli_error($con));
	                    
	            if($delete_query == true)
	            {
	                $_SESSION['delete'] = 1;
	                header("location: ".$_SERVER['REQUEST_URI']);
	            }
	        }
	    }
	}
		//STUDENT ACTIVITY

	if(isset($_POST['btn_deletestudact']))
	{
	    if(isset($_POST['chk_delete']))
	    {
	        foreach($_POST['chk_delete'] as $value)
	        {
	            $delete_query = mysqli_query($con,"DELETE from tblstudactivities where id = '$value' ") or die('Error: ' . mysqli_error($con));
	                    
	            if($delete_query == true)
	            {
	                $_SESSION['delete'] = 1;
	                header("location: ".$_SERVER['REQUEST_URI']);
	            }
	        }
	    }
	}

	//MESSAGE

	if(isset($_POST['btn_deletemess']))
	{

		//INBOX
	    if(isset($_POST['chk_delete']))
	    {
	        foreach($_POST['chk_delete'] as $value)
	        {
	            $delete_query = mysqli_query($con,"DELETE from tblinbox where id = '$value' ") or die('Error: ' . mysqli_error($con));
	                    
	            if($delete_query == true)
	            {
	                $_SESSION['delete'] = 1;
	                unset($_POST['btn_deletemess']);
	                header("location: ".$_SERVER['REQUEST_URI']);
	            }
	        }
	    }

	    //SENT
	    if(isset($_POST['chk_deletesent']))
	    {
	        foreach($_POST['chk_deletesent'] as $value)
	        {
	            $delete_query = mysqli_query($con,"DELETE from tblsent where id = '$value' ") or die('Error: ' . mysqli_error($con));
	                    
	            if($delete_query == true)
	            {
	                $_SESSION['delete'] = 1;
	                unset($_POST['btn_deletemess']);
	                header("location: ".$_SERVER['REQUEST_URI']);
	            }
	        }
	    }
	}
	
	//STUDENT MESSAGE

	if(isset($_POST['btn_sdeletemess']))
	{

		//INBOX
	    if(isset($_POST['chk_delete']))
	    {
	        foreach($_POST['chk_delete'] as $value)
	        {
	            $delete_query = mysqli_query($con,"DELETE from tblstudinbox where id = '$value' ") or die('Error: ' . mysqli_error($con));
	                    
	            if($delete_query == true)
	            {
	                $_SESSION['delete'] = 1;
	                unset($_POST['btn_sdeletemess']);
	                header("location: ".$_SERVER['REQUEST_URI']);
	            }
	        }
	    }

	    //SENT
	    
	    if(isset($_POST['chk_deletesent']))
	    {
	        foreach($_POST['chk_deletesent'] as $value)
	        {
	            $delete_query = mysqli_query($con,"DELETE from tblstudsent where id = '$value' ") or die('Error: ' . mysqli_error($con));
	                    
	            if($delete_query == true)
	            {
	                $_SESSION['delete'] = 1;
	                unset($_POST['btn_sdeletemess']);
	                header("location: ".$_SERVER['REQUEST_URI']);
	            }
	        }
	    }
	}

	//FACSTUDENT MESSAGE

	if(isset($_POST['btn_fsdeletemess']))
	{

		//INBOX
	    if(isset($_POST['chk_delete']))
	    {
	        foreach($_POST['chk_delete'] as $value)
	        {
	            $delete_query = mysqli_query($con,"DELETE from tblstudfacinbox where id = '$value' ") or die('Error: ' . mysqli_error($con));
	                    
	            if($delete_query == true)
	            {
	                $_SESSION['delete'] = 1;
	                unset($_POST['btn_fsdeletemess']);
	                header("location: ".$_SERVER['REQUEST_URI']);
	            }
	        }
	    }

	    //SENT
	    if(isset($_POST['chk_deletesent']))
	    {
	        foreach($_POST['chk_deletesent'] as $value)
	        {
	            $delete_query = mysqli_query($con,"DELETE from tblstudfacsent where id = '$value' ") or die('Error: ' . mysqli_error($con));
	                    
	            if($delete_query == true)
	            {
	                $_SESSION['delete'] = 1;
	                unset($_POST['btn_fsdeletemess']);
	                header("location: ".$_SERVER['REQUEST_URI']);
	            }
	        }
	    }
	}
	//STUDENTFAC MESSAGE

	if(isset($_POST['btn_sfdeletemess']))
	{

		//INBOX
	    if(isset($_POST['chk_delete']))
	    {
	        foreach($_POST['chk_delete'] as $value)
	        {
	            $delete_query = mysqli_query($con,"DELETE from tblstudfacinbox where id = '$value' ") or die('Error: ' . mysqli_error($con));
	                    
	            if($delete_query == true)
	            {
	                $_SESSION['delete'] = 1;
	                unset($_POST['btn_sfdeletemess']);
	                header("location: ".$_SERVER['REQUEST_URI']);
	            }
	        }
	    }

	    //SENT
	    if(isset($_POST['chk_deletesent']))
	    {
	        foreach($_POST['chk_deletesent'] as $value)
	        {
	            $delete_query = mysqli_query($con,"DELETE from tblstudfacsent where id = '$value' ") or die('Error: ' . mysqli_error($con));
	                    
	            if($delete_query == true)
	            {
	                $_SESSION['delete'] = 1;
	                unset($_POST['btn_sfdeletemess']);
	                header("location: ".$_SERVER['REQUEST_URI']);
	            }
	        }
	    }
	}

	//TEACHER FILE

	if(isset($_POST['btn_deletetfile']))
	{
	    if(isset($_POST['chk_delete']))
	    {
	        foreach($_POST['chk_delete'] as $value)
	        {
	            $delete_query = mysqli_query($con,"DELETE from tblteacherfile where id = '$value' ") or die('Error: ' . mysqli_error($con));
	                    
	            if($delete_query == true)
	            {
	                $_SESSION['delete'] = 1;
	                header("location: ".$_SERVER['REQUEST_URI']);
	            }
	        }
	    }
	}

	//STUDENT FILE

	if(isset($_POST['btn_deletetstudfile']))
	{
	    if(isset($_POST['chk_delete']))
	    {
	        foreach($_POST['chk_delete'] as $value)
	        {
	            $delete_query = mysqli_query($con,"DELETE from tblsudentfile where id = '$value' ") or die('Error: ' . mysqli_error($con));
	                    
	            if($delete_query == true)
	            {
	                $_SESSION['delete'] = 1;
	                header("location: ".$_SERVER['REQUEST_URI']);
	            }
	        }
	    }
	}




?>
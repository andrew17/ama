<div class="modal fade" id="AccountModal" tabindex="-1" role="dialog">
<form method="post" enctype="multipart/form-data" >
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="defaultModalLabel">Account</h4>
            </div>
            <div class="modal-body">
                <div class="row clearfix">
                    <div class="col-sm-12">
                        <?php
                        if($_SESSION['role'] == "Administrator")
                        {
                            $q1 = mysqli_query($con, "SELECT * from tbladmin where id = '".$_SESSION['userid']."' ");
                        }
                        else if ($_SESSION['role'] == "Faculty")
                        {
                            $q1 = mysqli_query($con, "SELECT * from tblfaculty where id = '".$_SESSION['userid']."' ");
                        }
                        else if ($_SESSION['role'] == "Student")
                        {
                            $q1 = mysqli_query($con, "SELECT * from tblstudent where id = '".$_SESSION['userid']."' ");echo '';
                        }
                        while($row = mysqli_fetch_array($q1)){
                            if ($_SESSION['role'] == "Administrator")
                            {
                                echo '
                                <div class="form-group">
                                    <label>Username:</label>
                                    <div class="form-line">
                                        <input name="txt_uname" type="text" class="form-control" value="'.$row['username'].'" placeholder="Username">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Password:</label>
                                    <div class="form-line">
                                        <input name="txt_pass" type="password" class="form-control" value="'.$row['password'].'" placeholder="Password">
                                    </div>
                                </div>';
                            }
                            else if($_SESSION['role'] == "Faculty")
                            {
                                echo '
                                    <div class="form-group">
                                    <label>Photo:</label>
                                    <div class="form-line">
                                        <input name="image" type="file">
                                        <img src='.$row['photo'].' width="70" height="70" style="margin-top:-50px; margin-left: 480px">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Lastname:</label>
                                    <div class="form-line">
                                        <input name="txt_funame" type="text" disabled class="form-control" value="'.$row['lname'].'" placeholder="Lastname" >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Firstname:</label>
                                    <div class="form-line">
                                        <input name="txt_ffname" type="text" disabled class="form-control" value="'.$row['fname'].'" placeholder="Firstname">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Middlename:</label>
                                    <div class="form-line">
                                        <input name="txt_fmname" type="text" disabled class="form-control" value="'.$row['mname'].'" placeholder="Middlename">
                                    </div>
                                </div>
                               
                                    <div class="form-group">
                                    <label>Address:</label>
                                    <div class="form-line">
                                        <input name="txt_faddress" type="text" disabled class="form-control" value="'.$row['address'].'" placeholder="Address">
                                    </div>
                                </div>

                                    <div class="form-group">
                                    <label>Contact:</label>
                                    <div class="form-line">
                                        <input name="txt_fcontact" type="number" class="form-control" value="'.$row['contact'].'" placeholder="Contact" >
                                    </div>
                                </div>

                                    <div class="form-group">
                                    <label>Email:</label>
                                    <div class="form-line">
                                        <input name="txt_femail" type="email" class="form-control" value="'.$row['email'].'" placeholder="Email" >
                                    </div>
                                </div> 

                                <div class="form-group">
                                    <label>Username:</label>
                                    <div class="form-line">
                                        <input name="txt_uname" type="text" class="form-control" value="'.$row['username'].'" placeholder="Username">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Password:</label>
                                    <div class="form-line">
                                        <input name="txt_pass" type="password" class="form-control" value="'.$row['password'].'" placeholder="Password">
                                    </div>
                                </div>';
                            }
                            else if ($_SESSION['role'] == "Student")
                            {
                                echo '
                                    <div class="form-group">
                                    <label>Photo:</label>
                                    <div class="form-line">
                                        <input name="image" type="file" >
                                        <img src='.$row['photo'].' width="70" height="70" style="margin-top:-50px; margin-left: 480px">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>USN:</label>
                                    <div class="form-line">
                                        <input name="txt_uname" type="text" class="form-control" value="'.$row['username'].'" placeholder="Username" disabled>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Lastname:</label>
                                    <div class="form-line">
                                        <input name="txt_suname" type="text" disabled class="form-control" value="'.$row['lname'].'" placeholder="Lastname ">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Firstname:</label>
                                    <div class="form-line">
                                        <input name="txt_sfname" type="text" disabled class="form-control" value="'.$row['fname'].'" placeholder="Firstname">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Middlename:</label>
                                    <div class="form-line">
                                        <input name="txt_smname" type="text" disabled class="form-control" value="'.$row['mname'].'" placeholder="Middlename">
                                    </div>
                                </div>
                               
                                    <div class="form-group">
                                    <label>Address:</label>
                                    <div class="form-line">
                                        <input name="txt_saddress" type="text" disabled class="form-control" value="'.$row['address'].'" placeholder="Address">
                                    </div>
                                </div>

                                    <div class="form-group">
                                    <label>Contact:</label>
                                    <div class="form-line">
                                        <input name="txt_scontact" type="number" class="form-control" value="'.$row['contact'].'" placeholder="Contact">
                                    </div>
                                </div>

                                    <div class="form-group">
                                    <label>Email:</label>
                                    <div class="form-line">
                                        <input name="txt_semail" type="email" class="form-control" value="'.$row['email'].'" placeholder="Email">
                                    </div>
                                </div>  

                                <div class="form-group">
                                    <label>Password:</label>
                                    <div class="form-line">
                                        <input name="txt_pass" type="password" class="form-control" value="'.$row['password'].'" placeholder="Password">
                                    </div>
                                </div>';
                            }
                        }

                        ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-link waves-effect" name="btn_acct" >SAVE</button>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</form>
</div>

<?php
            
            if(isset($_POST['btn_acct'])){
                $uname = $_POST['txt_uname'];
                $pass = $_POST['txt_pass'];

                if($_SESSION['role'] == "Administrator"){
                    $u = mysqli_query($con, "UPDATE tbladmin set username = '".$uname."', password = '".$pass."' where id = '".$_SESSION['userid']."' ");
                }
                if($u == true){
                    $_SESSION['account'] = 1;
                    header ("location: ".$_SERVER['REQUEST_URI']);
                    exit;
                }
            }
            if(isset($_POST['btn_acct'])){
                $uname = $_POST['txt_uname'];
                $pass = $_POST['txt_pass'];
                $fcons = $_POST['txt_fcontact'];
                $femas = $_POST['txt_femail'];

                if($_FILES['image']['error'] > 0) { echo 'Error during uploading, try again'; }
                                    $extsAllowed = array( 'jpg', 'jpeg', 'png', 'gif' );

                                    $extUpload = strtolower( substr( strrchr($_FILES['image']['name'], '.') ,1) ) ;
                                        if (in_array($extUpload, $extsAllowed) ) { 
                                            $name = "../upload/{$_FILES['image']['name']}";
                                            $result = move_uploaded_file($_FILES['image']['tmp_name'], $name);

                                                if($result){echo "<img src='$name'/>";}
                                                } else { echo 'File is not valid. Please try again'; }
 
                if($_SESSION['role'] == "Faculty"){
                    $u = mysqli_query($con, "UPDATE tblfaculty set contact = '".$fcons."', email = '".$femas."', photo = '".$name."', username = '".$uname."', password = '".$pass."' where id = '".$_SESSION['userid']."' ");
                }

                if($u == true){
                    $_SESSION['account'] = 1;
                    header ("location: ".$_SERVER['REQUEST_URI']);
                    exit;
                }
            }
             if(isset($_POST['btn_acct'])){
                
                $pass = $_POST['txt_pass'];
                $cons = $_POST['txt_scontact'];
                $emas = $_POST['txt_semail'];
                $email = $_POST['email'];

                if($_FILES['image']['error'] > 0) { echo 'Error during uploading, try again'; }
                                    $extsAllowed = array( 'jpg', 'jpeg', 'png', 'gif' );

                                    $extUpload = strtolower( substr( strrchr($_FILES['image']['name'], '.') ,1) ) ;
                                        if (in_array($extUpload, $extsAllowed) ) { 
                                            $name = "../upload/{$_FILES['image']['name']}";
                                            $result = move_uploaded_file($_FILES['image']['tmp_name'], $name);

                                                if($result){echo "<img src='$name'/>";}
                                                } else { echo 'File is not valid. Please try again'; }
 

                if ($_SESSION['role'] == "Student"){
                    $u = mysqli_query($con, "UPDATE tblstudent set contact = '".$cons."', email = '".$emas."', photo = '".$name."', password = '".$pass."' where id = '".$_SESSION['userid']."' ");
                }

                if($u == true){
                    $_SESSION['account'] = 1;
                    header ("location: ".$_SERVER['REQUEST_URI']);
                    exit;
                }
            }    

            ?>
            
<!DOCTYPE html>
<html>

<?php
session_start();
?>
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>AMA: OFMS (Online File Management System)</title>
    <!-- Favicon-->

    <!-- Google Fonts -->
    <link href="../css/font.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="../plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="../plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="../plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="../css/style.css" rel="stylesheet">

<style>

.panel-body {
    padding: 15px;
    background-color: #fff;
}
.navbar-default {
    border-color: #dadada;
}
.panel-default > .panel-heading {
    color: #f2f2f2;
    background-color: #af0808;
    border-color: #dadada;
    text-align: center;
}

body {
    width: 100wh;
    height: 90vh;
    color: #fff;
    background-image: url(../images/panel.png);
    background-size: 130% 130%;
    -webkit-animation: Gradient 30s ease infinite;
    -moz-animation: Gradient 30s ease infinite;
    animation: Gradient 30s ease infinite;
}


.logo-margin {
    margin-top: 100px;
}
.login-panel {
    margin-top: 65%;
}
.btn-success{
    background-color: #af2424 !important;
}
.btn-success:hover{
    background-color:#af0808 !important;
}
.ama{
    position: absolute;   
    width: 100%;
    left: 0;
    top: 0%;
    text-align: center;
    margin-top: 50px;
}
#am{
    width:600px; 
    height:220px;
}
.ima1{
    position: absolute;
    right: 220px;
    bottom:40px;

}
.ima2{
    position: absolute;
    right: 0px;
    bottom: 0px;
}
.foot{
    position: absolute;
    left: 0px;
    bottom: 0px;
    margin: auto;
}
.foot p {

    color: #fff;
    
}
img{
    width: 100%;
    height: auto;
}
</style>
</head>
<body class="body-Login-back">
    <div class="container">
        <div class="ama">
            <img id="am" src="../images/tg1.png">
        </div>
        <div class="row">
            <div class="ima1 ">
            </div>
            <div class="ima2">

            </div>
            <br><br>
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Sign In</h3>
                    </div>
                    <div class="panel-body">
                        
                        <form role="form" method="post">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control input-m" placeholder="Username/USN" style="text-align:center;" name="username" type=x"text" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control input-m" placeholder="Password" style="text-align:center;" name="password" type="password" value="">
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <input type="submit" class="btn btn-m btn-success btn-block" name="btn_submit" value="Login">
                                <!-- <a href="main.php" class="btn btn-lg btn-success btn-block">Login</a> -->
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
        $con = mysqli_connect('localhost','root','','db_amafile') or die(mysqli_error());
        date_default_timezone_set("Asia/Manila");

        

        if(isset($_POST['btn_submit'])){
            $username = $_POST['username'];
            $password = $_POST['password'];

            $a = mysqli_query($con,"SELECT * from tbladmin where username = '".$username."' and password = '".$password."' ");
            $ad = mysqli_num_rows($a);

            $f = mysqli_query($con,"SELECT * from tblfaculty where username = '".$username."' and password = '".$password."' ");
            $fa = mysqli_num_rows($f);

            $c = mysqli_query($con,"SELECT * from tblstudent where username = '".$username."' and password = '".$password."' ");
            $ca = mysqli_num_rows($c);

            if($ad > 0){
                while($row = mysqli_fetch_array($a)){
                  $_SESSION['role'] = "Administrator";
                  $_SESSION['userid'] = $row['id'];
                  $_SESSION['username'] = $row['username'];
                }    
                header ('location: dashboard/dashboard.php');
            }
            else if($fa > 0){
                while($row = mysqli_fetch_array($f)){
                  $_SESSION['role'] = "Faculty";
                  $_SESSION['userid'] = $row['id'];
                  $_SESSION['username'] = $row['username'];
                }    
                header ('location: dashboard/dashboard.php');
            }
            else if($ca > 0){
                while($row = mysqli_fetch_array($c)){
                  $_SESSION['role'] = "Student";
                  $_SESSION['userid'] = $row['id'];
                  $_SESSION['username'] = $row['username'];
                }    
                header ('location: dashboard/dashboard.php');
            }
        }

    ?>
    <!-- Jquery Core Js -->
    <script src="../plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="../plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="../plugins/node-waves/waves.js"></script>

    <!-- Validation Plugin Js -->
    <script src="../plugins/jquery-validation/jquery.validate.js"></script>

    <!-- Custom Js -->
    <script src="../js/admin.js"></script>
    <script src="../js/pages/examples/sign-in.js"></script>
</body>
          <!-- <footer>
    <div class="foot">

        <p>&copy; 2018, Andrew Valorozo & Ahley Tomas <a href="#">AMA Online File Management System</a> </p>
    </div>
</footer> -->
</html>